import psycopg2
import json
import logging
import traceback
import boto3
import base64
from uuid import uuid4

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def get_secret(secret_name):
    region_name = "us-east-2"

    logger.info("Connecting to secret manager.")
    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name)

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
        logger.info(get_secret_value_response)
    except:
        logger.error(f'ERROR: Exception connecting to secret manager \n{traceback.format_exc()}.')

    else:
        if 'SecretString' in get_secret_value_response:
            secret = get_secret_value_response['SecretString']
        else:
            secret = base64.b64decode(get_secret_value_response['SecretBinary'])

        logger.info(secret)

        return json.loads(secret)


def handler(event, context):

    master_secret_name = event['secret_id']

    db_info = get_secret(master_secret_name)
    logger.info(f"Successfully got to the db Info: {db_info}")

    sql_query = event["query"]
    dbname = db_info['dbInstanceIdentifier']
    try:
        conn = psycopg2.connect(
            user=db_info["username"],
            password=db_info["password"],
            host=db_info["host"],
            port=db_info["port"],
            database=dbname,
            sslmode="require",
            connect_timeout=10)

        conn.autocommit = True
        cursor = conn.cursor()

        logger.info(f"Started executing SQL: {sql_query}.")
        cursor.execute(sql_query)
        logger.info(f"Query: {sql_query} - executed with success.")
        logger.info(f"status message: {cursor.statusmessage}.")

        exists = cursor.fetchall()
        logger.info(exists)
        desc = cursor.description
        logger.info(f"cursor description: {desc}.")
        logger.info("data fetched from cursor:")

        if exists:
            for row in exists:
                logger.info(row)
        else:
            logger.info("Nothing fetched from cursor.")
    except psycopg2.ProgrammingError as e:
        logger.info(f"Nothing to fetch from executed query, {str(e)}.")
    except Exception as e:
        logger.info(f"ERROR: Cannot fetched data from cursor while executing query.{str(e)}")
    finally:
        try:
            conn.close()
        except:
            pass
