locals {
  aurora_db_subnets = [aws_subnet.aurora_db_subnet_2a.id, aws_subnet.aurora_db_subnet_2b.id]
}


resource "aws_vpc_endpoint" "secret_manager_endpoint" {
  vpc_id            = aws_vpc.aurora_db_vpc.id
  subnet_ids        = local.aurora_db_subnets
  service_name      = "com.amazonaws.us-east-2.secretsmanager"
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    aws_security_group.secret_manager_endpoint_sg.id
  ]
  private_dns_enabled = true
}
