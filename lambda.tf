resource "aws_lambda_permission" "allow_secret_manager_invoke" {
  statement_id  = "AllowSecretManagerInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.handler_lambda.function_name
  principal     = "secretsmanager.amazonaws.com"
}

resource "aws_iam_role" "lambda_iam_role" {
  name = "lambda_iam_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "lambda_iam_policy" {
  name   = "lambda_iam_role_policy"
  policy = file("lambda_iam_policy.json")
}

resource "aws_iam_policy_attachment" "lambda_iam_policy_attach" {
  name       = "lambda_iam_policy_attach"
  roles      = [aws_iam_role.lambda_iam_role.name]
  policy_arn = aws_iam_policy.lambda_iam_policy.arn
}

resource "aws_lambda_function" "handler_lambda" {
  filename      = "function.zip"
  function_name = "handler_lambda"
  role          = aws_iam_role.lambda_iam_role.arn
  handler       = "function.handler"

  source_code_hash = filebase64sha256("function.zip")

  runtime = "python3.7"

  vpc_config {
    subnet_ids         = [aws_subnet.aurora_db_subnet_2a.id, aws_subnet.aurora_db_subnet_2b.id]
    security_group_ids = [aws_security_group.secret_manager_endpoint_sg.id]
  }

  environment {
    variables = {
      SECRETS_MANAGER_ENDPOINT = "https://secretsmanager.us-east-2.amazonaws.com"
    }
  }

  depends_on = [
    module.aurora.this_rds_cluster_arn
  ]
}
