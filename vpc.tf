locals {
  rds_vpc_cidr  = "10.10.0.0/16"
  rds_subnet_2a = "10.10.10.0/24"
  rds_subnet_2b = "10.10.20.0/24"
}

resource "aws_vpc" "aurora_db_vpc" {
  cidr_block           = local.rds_vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "aurora_db_vpc"
  }
}

resource "aws_subnet" "aurora_db_subnet_2a" {
  vpc_id            = aws_vpc.aurora_db_vpc.id
  cidr_block        = local.rds_subnet_2a
  availability_zone = "us-east-2a"
  tags = {
    Name = "aurora_db_subnet_2a"
  }
}

resource "aws_subnet" "aurora_db_subnet_2b" {
  vpc_id            = aws_vpc.aurora_db_vpc.id
  cidr_block        = local.rds_subnet_2b
  availability_zone = "us-east-2b"
  tags = {
    Name = "aurora_db_subnet_2b"
  }
}
