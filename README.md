# Terraform

Terraform is Infrastructure as Code technology (IaC) that is widely used for managing cloud infrastructure.

During the workshop Terraform will be introduced together with AWS.  
Together, we will create a database (PostgreSQL) with serverless technologies like Lambda to setup tables and schemas.
AWS CloudWatch will be used for monitoring.

We will see how Terraform simplifies environment management, reduces manual work, and encourages code reuse within teams and the developer community.

 

Key words: Terraform, IaC, AWS, Lambda, CloudWatch, RDS Aurora PostgreSQL.


Authors:
Malgorzata Mateusiak & Agata Grzanka
