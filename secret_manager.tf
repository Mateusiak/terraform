locals {
  postgres_secret = {
    engine   				= local.database_engine
    dbInstanceIdentifier   	= local.database_name
    host     				= data.dns_a_record_set.aurora_postgresql_ip.addrs[0]
    username 				= module.aurora.this_rds_cluster_master_username
    password 				= module.aurora.this_rds_cluster_master_password
    port     				= module.aurora.this_rds_cluster_port
  }
  postgres_secret_name = "aurora/postgres/dbpassword"
}

resource "aws_secretsmanager_secret" "postgres_secret" {
  name_prefix = local.postgres_secret_name
}

resource "aws_secretsmanager_secret_version" "postgres_secret_value" {
  secret_id     = aws_secretsmanager_secret.postgres_secret.id
  secret_string = jsonencode(local.postgres_secret)
}
