locals {
  database_name   = "aurora_postgresql"
  database_engine = "aurora-postgresql"
}

module "aurora" {
  source  = "git::https://github.com/terraform-aws-modules/terraform-aws-rds-aurora?ref=v2.29.0"

  name                            = "aurora"
  database_name                   = local.database_name
  engine                          = local.database_engine
  engine_version                  = "11.6"
  subnets                         = [aws_subnet.aurora_db_subnet_2a.id, aws_subnet.aurora_db_subnet_2b.id]
  allowed_cidr_blocks             = [local.rds_subnet_2a, local.rds_subnet_2b]
  vpc_id                          = aws_vpc.aurora_db_vpc.id
  replica_count                   = 1
  instance_type                   = "db.t3.medium"
  apply_immediately               = true
  skip_final_snapshot             = true
  db_parameter_group_name         = aws_db_parameter_group.aurora_db_postgres11_parameter_group.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.aurora_cluster_postgres11_parameter_group.id
  security_group_description      = "Postgres security group"
  username                        = "master"
  password                        = "alamakota"
}

data "dns_a_record_set" "aurora_postgresql_ip" {
  host       = module.aurora.this_rds_cluster_endpoint
  depends_on = [module.aurora.this_rds_cluster_instance_ids]
}

resource "aws_db_parameter_group" "aurora_db_postgres11_parameter_group" {
  name        = "test-aurora-db-postgres11-parameter-group"
  family      = "aurora-postgresql11"
  description = "test-aurora-db-postgres11-parameter-group"
}

resource "aws_rds_cluster_parameter_group" "aurora_cluster_postgres11_parameter_group" {
  name        = "test-aurora-postgres11-cluster-parameter-group"
  family      = "aurora-postgresql11"
  description = "test-aurora-postgres11-cluster-parameter-group"
}




