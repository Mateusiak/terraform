
resource "aws_security_group" "secret_manager_endpoint_sg" {
  name        = "Allow HTTPS"
  description = "Allow HTTPS"
  vpc_id      = aws_vpc.aurora_db_vpc.id

  ingress {
    description = "Allow HTTPS connect"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.aurora_db_subnet_2a.cidr_block, aws_subnet.aurora_db_subnet_2b.cidr_block]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_https_connect"
  }
}